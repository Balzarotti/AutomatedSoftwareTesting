
* Lesson 1:

Software testing is
- 
Fault = defect (the one to be detected by the tests), mistake in the code that causes some trouble; failure
consequence of a fault

Bug: is informal, better use fault and failure
We’ll not talk about verification but validation (= software testing)

Various level of testing:
- Unit
- Integration
- System: use of the interface
- Acceptance: contractual

When we have tested enough, there’s the term of adequacy of testing;
different criteria.
- Functional coverage: each function at least once
- Structural coverage: all the
  - Statement coverage: every statement at least once
- Mutation coverage: injecting artificial bugs in the program. Those
  artificial fault (mutant) must be exposed during testing at least
  once.

Paper: Matt Staats, Mats Per Erik: Progams, Tests, and oracles: the
foundations of testing revisited, ICS 2011

 Software: junit, spoon

** Search Algorithms
Search problem = maximiazion/minimization problem

** Assignment
- Triangle Class
- Goal: Auto generate statement test cases with genetic algorithm.
  Fixed (3 element) length chromosome

Fitness for statement/branch coverage:
=f(x) = approch_level(P(x),t) + branch_distance(P(x), t)=s

1. Approch level: number of intermediate node from the execution trace
   and the target
2. Branch distance: Predicate of the control node which is closest to
   the target converted to a distance, which measures how far the
   testa case is from taking the desired branch,





* Lesson 2:
- Object oriented testing
- Dynamic symbolic execution

** Develop a class: how to write the junit
- Instantiate a class
- Method invocation to bring it to the desired state

** Defensive programming: Best practice

Check something that cannot happen but that will case failure if
somebody in the future allows it.

** Entire test suite

Minimize the sum of all the fitness values

** NO inter-dependent tests
Setup phase (deterministic point of the system) + Teardown phase
(return to original state)

** Whole system test -> Many objective test case generation

*** As in whole system
All targets added at the same time

~Sum scalarization~ : the winner ends up not being the best, but the
best average (may work worst on other some branch) -> separate fitness
functions

*** many objective test case generation
Individuals in the many objective test case are based on single test
cases

MOSA: included in [[http://www.evosuite.org/][evosuite]]

** Dynamic Symbolic Execution
Completely different approach. The alternative is the ~static~, so
we’ll start talking about it.

Propagate symbolic input values for the program. Create a symbolic
path execution and find values that propagates.

The path condition is got by computing a symbolic expression that is a
function of the input, and solving this.

* Lesson 3: Grammar based Testing

** Introduction

System that accepts highly structured input (ie. Compilers/Web
browser/PDF/Robot control system)

Architecture:
Text input -> Lexical analyzer (token sequence) -> Syntax analyzer ->
Abstract Syntax Tree -> Business Logic (ie. everything that knows how
to render, like the assembly code)

ANTLR: tool to build compilers/interpreters… Token defined as Regex

The language is potentially infinite. Grammars have recursive
rules. BUT usually grammars are defined with LEX/YACC -> common
grammar available in (eg., in BNF)

Approaches:
- Coverage
- Random generation
- Other

** Coverage-based methods

Purdom’s algorithm. Small set of sentences that exercise every rule
in a given context free grammar.



** Random Generation Methods

** Dealing with Semantic

** RNN


* Lesson 4: difficulties in software testing

** Meta
1. Web apps are the assets of our society
2. 



