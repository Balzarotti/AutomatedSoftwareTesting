using JuliaParser
src = readstring("ex.jl")
ast = Parser.parse(src);

function deepen(x::Expr, new::Expr, farr::Array{Function}, indent::Int)
    newexpr = :()
    # add head
    newexpr.head = x.head
    for arg in x.args
        if isa(arg,Expr)
            deepen(arg, newexpr, farr, indent + 1)
        else
            push!(newexpr.args, arg)
        end
    end
    map(y -> y(x, newexpr, indent), farr)
    push!(new.args, newexpr)
end

modified = :()
function inject_into_if(x, newexpr, indent)
    if x.head == :if
        unshift!((newexpr.args[2].args), :(println("injected code at level ",$indent)))
    end
end
function inject_into_while(x, newexpr, indent)
    if x.head == :while
        unshift!((newexpr.args[2].args), :(println("This while is at level ",$indent)))
    end
end

function extract_conditions(dest, condition::Symbol, x, newexpr, indent)
    if x.head == condition
        push!(dest,x.args[1])
    end
end

if_conditions = []
extract_if_conditions(args...) = extract_conditions(if_conditions, :if, args...)
while_conditions = []
extract_while_conditions(args...) = extract_conditions(while_conditions, :while, args...)

fAr = Array{Function}([ inject_into_if, inject_into_while, extract_if_conditions, extract_while_conditions ])

deepen(ast, modified, fAr, 0)

modified

eval(modified); main(1,2)
