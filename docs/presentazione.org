#+TITLE: Automated Software Testing
#+AUTHOR: Nicolò Balzarotti
#+EMAIL: n.balzarotti@iit.it, n.balzarotti@studenti.unisr.it
#+DATE: 2017-05-12
#+DESCRIPTION:
#+KEYWORDS: 
#+LANGUAGE:  en
#+STARTUP: beamer
#+LaTeX_CLASS: beamer
#+BEAMER_THEME: metropolis
#+LaTeX_CLASS_OPTIONS: [presentation,smaller,aspectratio=43]
#+latex_header: \mode<beamer>{\usetheme{metropolis}}
#+LATEX_HEADER: \RequirePackage{fancyvrb}
#+LATEX_HEADER: \DefineVerbatimEnvironment{verbatim}{Verbatim}{fontsize=\scriptsize}
#+OPTIONS:   H:2 num:t toc:t 
#+OPTIONS:   TeX:t LaTeX:t skip:nil d:nil todo:t pri:nil tags:not-in-toc

* A short Introduction

** Objectives and Software

The aim of this presentation is to learn how to write an *Automated
Software Test* generation in *Java* and to report it’s results.

The software that have been used are:
- The ~Java~ Compiler (~OpenJDK 1.8~)
- [[http://spoon.gforge.inria.fr/][~spoon~]] - Source Code Analysis and Transformation for Java
- Build Automation:
  - ~Gradle~: a ~Java Build System~
  - ~PHP~: Programming Language - Java ~JUnit4~ test pre-processing
  - ~entr~: run arbitrary commands when file change - automatically
   run gradle and the ~PHP~ post-processing
- Data Analysis:
  - [[https://julialang.org][~julia~]] programming language
- Plots:
  - ~Plots.jl~ (~GR~ backend)
- Presentation: GNU/Linux, [[http://nixos.org/][~NixOS~]], emacs, [[http://orgmode.org/][~org-mode~]] (emacs), beamer,
  metropolis theme
  # , ~dot~ (DSL)

** A Note on Spoon

Relevant code on spoon fitness value injection
#+BEGIN_SRC java
  @Override
  public boolean isToBeProcessed(CtStatement c) {
      return c.getPosition().getLine() == target;
  }
#+END_SRC
#+BEGIN_SRC java
  do {
      if (el instanceof CtBlock) {
          AST.add((CtBlock)el);
      }
      el = el.getParent();
      if (el instanceof CtClass) {
          CtClass cl = (CtClass)el;
          addFitnessVariableField(cl);
      }
  }
#+END_SRC
#+BEGIN_SRC java
  if (e instanceof CtBlock) {
      CtCodeSnippetStatement snippet = getFactory().Core().createCodeSnippetStatement();
      snippet.setValue("fitness = Math.min(fitness, " + depth + ")");
      CtBlock b = (CtBlock)e;
      b.insertBegin(snippet);
      depth+=1;
  }
#+END_SRC

** Algorithms and Implementations

I compared 3 different algorithms[fn:1] for test-case generation:
- [[file+emacs:~/Scm/Git/J2me/Assignments/demo/src/test/java/BinarySearchProcessedTest.java::private static int RandomGenerator][*Random Data*]]
- [[file+emacs:~/Scm/Git/J2me/Assignments/demo/src/test/java/BinarySearchProcessedTest.java::private static int HillClimb][*Hill Climbing*]]
  1. Gen an array of 6 random arrays (=int [6][6]=)
  2. Evaluate one by one, break if resource consumed
     1. If =new_fitness= < =best_fitness=, break
     2. Gen 6 neighbors, resume from 2 until 0 reached
- [[file+emacs:~/Scm/Git/J2me/Assignments/demo/src/test/java/BinarySearchProcessedTest.java::private static int Genetic][*Genetic*]]
  1. Gen an array of 6 random arrays (=int [6][6]=)
  2. Evaluate fitness for all of them (6) (end if resources consumed)
     1. Fill the array with the top 3 rated
     2. Apply *crossover* (with one random array in the 6, random cut point [1;4]) with ~p < 80%~
     3. Apply random *mutation* with ~p < 10%~ (change value with [0;49])
  3. Resume from 1 until 0 is reached

* Data Analysis

** Algorithm comparison

The following command has been issued:
#+BEGIN_SRC bash
  for i in 10 100 1000 10000
  do
      java -cp build/classes/test/:build/classes/main/ BinarySearchProcessedTest 1000 $i
  done
#+END_SRC
The first parameter (1000) is the number of generation to run (for
each generator algorithm). The second parameter ($i,
10,100,1000,1000), is the number of resources available (how easily
can a bad algorithm catch-up with best just by using more resources?)

** RQ1 - Effectiveness

#+BEGIN_SRC julia :exports none :session default :dir ../Assignments/demo/outputs :eval never
  using HypothesisTests
  using DataFrames
#+END_SRC

#+RESULTS:

# We import and prepare the data
#+BEGIN_SRC julia :session default :exports none :eval never
  rc = readtable("Random.csv")
  hc = readtable("HillClimb.csv")

  contingency = DataFrame();
  contingency[:Random] = [sum(rc[:found] .== 1), sum(rc[:found] .== 0)]
  contingency[:HillClimb] = [sum(hc[:found] .== 1), sum(hc[:found] .== 0)]
  contingency[:state] = [:success, :fail]  
#+END_SRC

#+RESULTS:

#+BEGIN_SRC julia :session default :exports none :eval never
  contingency

  fisher = FisherExactTest(contingency[:Random][1], contingency[:Random][2],
                  contingency[:HillClimb][1], contingency[:HillClimb][2])
#+END_SRC
Resources = 10
| Random | HillClimb | Genetic | State   |
|--------+-----------+---------|---------|
|      8 |        12 |     351 | success |
|    992 |       988 |     649 | fail    |

Resources = 100
| Random | HillClimb | Genetic | State   |
|--------+-----------+---------+---------|
|    109 |       389 |    1000 | success |
|    891 |       611 |       0 | fail    |

Resources = 300
| Random | HinllClimb | Genetic | State   |
|--------+-----------+---------+---------|
|    230 |       798 |    1000 | success |
|    770 |       202 |       0 | fail    |

# Resources = 1000
# | Random | HillClimb | Genetic | State   |
# |--------+-----------+---------+---------|
# |    621 |       970 |    1000 | success |
# |    379 |        30 |       0 | fail    |

- Resources = 1000 - HillClimb reaches 97%
- Resources = 10000 - even random reaches 100%

** Statistics

Are the observed differences *significative*?

3 way comparison using *\chi-squared*

#+BEGIN_SRC julia :exports code :eval never
  ChisqTest([contingency100[:HillClimb] contingency100[:Random] contingency100[:Genetic]])
#+END_SRC
two-sided p-value:           0.0, reject h_0

Comparisons (using the *Exact Fisher’s Test*):
+ Random vs HillClimb: p-value < 2.720558827316723e-49
+ HillClimb vs Genetic: p-value < 1.2299161963060792e-244

** RQ2 - Efficiency

The mean resources consumed (given a maximum of ~10000~) is
|   Random | HillClimb | Genetic |
| 1065.845 |   229.044 |  16.314 |

#+BEGIN_SRC julia
  pvalue(ApproximateSignedRankTest(Array(res[:Random]), Array(res[:HillClimb])))
  pvalue(ApproximateSignedRankTest(Array(res[:HillClimb]), Array(res[:Genetic])))
#+END_SRC
Comparison (using Wilcoxon/Mann-Whitney U)
- Random vs HillClimb: 5.63769060178412e-27
- HillClimb vs Genetic: 2.2804702685503183e-164

** Some nice plot ;)

Same data, visualized:

file:../Assignments/demo/outputs/boxplot_noviolin.png

* Conclusion

** Comments, Possible Improvements

As we have seen, different generation algorithms have different
performances, even if this *trivial example*. The *Genetic* algorithm
converge _one order of magnitute faster_ then the *Hill Climbing*
(even if the selection method is extremely easy).
The same speedup holds true for the Hill Climbing in regard to the
*Random Value Generation* too. However, is it possible quite easily to
account for the inefficiency of the algorithms with a 15X waste of
running time.

We remember also that the Branch Distance has not been considered due
to the lack of time.

** Me, my current project & my future

- Master degree in *Psychology* at ~S.Raffaele University~ (Milan)
- *PhD* at ~University of Genova~ (~IIT~, ~RBCS~, ~dtilab~; tutor: Gabriel
  Baud-Bovy)

  PhD project: *WeDraw* european project - teaching *math&physics* to 6–10
  years-old children using multisensory information
  (*haptic*, vision, hearing).
- How this course will change my project (and my life):
  - Better knowledge of how manually–written tests should be
  - And.. my favorite programming language

** Julia
#+ATTR_LATEX: :width 1cm
[[./imgs/julia.png]]
It’s *lispy))))))))))* → metaprogramming is welcome

#+BEGIN_SRC julia
using JuliaParser
function deepen(x::Expr, new::Expr)
    newexpr = :()
    newexpr.head = x.head
    for arg in x.args
        if isa(arg,Expr)
            deepen(arg, newexpr)
        else
            push!(newexpr.args, arg)
        end
    end
    if x.head == :if
        unshift!((newexpr.args[2].args), :(println("injected code")))
    end
    push!(new.args, newexpr)
end
#+END_SRC

** Usage Example
#+BEGIN_SRC julia
src = readstring("ex.jl")
ast = Parser.parse(src);
modified = :()
deepen(ast, modified)
#+END_SRC
Code transformation!
#+BEGIN_SRC julia
function main(i::Int,j::Int)
    if i == 1
        println("condition true")
        if j == 1
            println("other condition true")
        end
    end
end
#+END_SRC
#+BEGIN_SRC julia
 :(function main(i::Int,j::Int) # none, line 2:
        if i == 1
            println("injected code") # none, line 3:
            println("condition true") # none, line 4:
            if j == 1
                println("injected code") # none, line 5:
                println("other condition true")
            end
        end
    end)
#+END_SRC

** Questions
:PROPERTIES:
:BEAMER_opt: label=findings,standout
:END:

You have questions? I (might) have answers!

* Footnotes

[fn:1] The code is on gitlab @Balzarotti/AutomatedSoftwareTesting
