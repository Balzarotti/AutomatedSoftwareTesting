#!/usr/bin/env bash

# java -classpath ./bin/demo-0.0.jar:./lib/spoon.jar spoon.Launcher -i ./src/main/java/BinarySearch.java -p org.inst.BlockInstrumentor

java -classpath ./bin/demo-0.0.jar:./lib/spoon.jar spoon.Launcher -i ./src/main/java/BinarySearch.java -p org.inst.Fitness
echo "Copy output to src"
cp ./spooned/eu/fbk/se/tcgen2/BinarySearchProcessed.java ./src/main/java/

echo "Adding the Array to JUnit"
php "./template/junit.template" BinarySearchProcessed "$(cat found_array)" search > JUNIT_TEST_AUTO.java
cp -n JUNIT_TEST_AUTO.java ./src/test/java/AutomatedTest.java
