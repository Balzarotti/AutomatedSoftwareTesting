package org.inst;

import java.util.HashSet;

import spoon.Launcher;
import spoon.SpoonAPI;
import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtCodeSnippetStatement;
import spoon.support.reflect.declaration.CtFieldImpl;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.code.CtBlock;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtVariable;
import spoon.reflect.declaration.ModifierKind;

import spoon.reflect.factory.TypeFactory;
import spoon.reflect.declaration.CtType;

public class BlockInstrumentor extends AbstractProcessor<CtClass<?>> {
    public static void main(String[] args) throws Exception {
        SpoonAPI spoon = new Launcher();
        spoon.process();
        spoon.prettyprint();
    }
    @Override
    public boolean isToBeProcessed(CtClass<?> c) {
        return true;
    }
    
    public void process(CtClass<?> c) {
    }
}
