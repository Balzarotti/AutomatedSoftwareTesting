package org.inst;

import java.util.HashSet;
import java.util.ArrayList;
import java.util.List;

import spoon.Launcher;
import spoon.SpoonAPI;
import spoon.processing.AbstractProcessor;
import spoon.reflect.code.CtCodeSnippetStatement;
import spoon.support.reflect.declaration.CtFieldImpl;
import spoon.reflect.declaration.CtClass;
import spoon.reflect.declaration.CtMethod;
import spoon.reflect.code.CtBlock;
import spoon.reflect.code.CtStatement;
import spoon.reflect.declaration.CtField;
import spoon.reflect.declaration.CtVariable;
import spoon.reflect.declaration.ModifierKind;
import spoon.reflect.declaration.CtElement;

import spoon.reflect.visitor.Filter;
import spoon.reflect.visitor.filter.TypeFilter;

import spoon.reflect.factory.TypeFactory;
import spoon.reflect.declaration.CtType;

public class Fitness extends AbstractProcessor<CtStatement> {
    public static void main(String[] args) throws Exception {
        SpoonAPI spoon = new Launcher();
        spoon.process();
        spoon.prettyprint();
    }
    @Override
    public boolean isToBeProcessed(CtStatement c) {
        return c.getPosition().getLine() == target;
    }
    
    private void addFitnessVariableField(CtClass c) {
        // Fitness variable
        final CtField<Integer> fitness = new CtFieldImpl<Integer>();
        fitness.setSimpleName("fitness");

        // Type: integer
        TypeFactory factory = new TypeFactory();
        fitness.setType(factory.INTEGER_PRIMITIVE);

        // Modifiers: Public, static
        final HashSet<ModifierKind> modifiers = new HashSet<ModifierKind>();
        modifiers.add(ModifierKind.STATIC);
        modifiers.add(ModifierKind.PUBLIC);
        fitness.setModifiers(modifiers);

        // Add to the class
        c.addField(fitness);

        c.setSimpleName(c.getSimpleName() + "Processed");
    }

    public void setTargetLine(int new_target) {
        target = new_target;
    }
    
    public void process(CtStatement c) {
        CtElement el;
        for (CtStatement state : c.getElements(stfilter)) {
            if (state.getPosition().getLine() == target) {
                el = state.getParent();
                do {
                    if (el instanceof CtBlock) {
                        AST.add((CtBlock)el);
                    }
                    el = el.getParent();
                    if (el instanceof CtClass) {
                        CtClass cl = (CtClass)el;
                        addFitnessVariableField(cl);
                    }
                } while (! (el instanceof CtClass));

                int depth = 0;
                for (CtElement e : AST) {
                    CtCodeSnippetStatement snippet = getFactory().Core().createCodeSnippetStatement();
                    if (e instanceof CtBlock) {
                        snippet.setValue("fitness = Math.min(fitness, " + depth + ")");
                        CtBlock b = (CtBlock)e;
                        b.insertBegin(snippet);
                        depth+=1;
                    }
                }
            } else {/*not found line*/}   
        }
    }

    private static Filter<CtBlock> blfilter = new TypeFilter<>(CtBlock.class);
    private static Filter<CtBlock> stfilter = new TypeFilter<>(CtStatement.class);
    private ArrayList<CtElement> AST = new ArrayList<CtElement>();
    private int target = 43; // Where to set?
}
