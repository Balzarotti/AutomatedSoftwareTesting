import org.junit.Test;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.nio.file.Files;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.charset.Charset;
import static org.junit.Assert.*;
import eu.fbk.se.tcgen2.BinarySearchProcessed;

public class BinarySearchProcessedTest {
    private static int max_fitness = 99999;
    private static int resources = 300;
    private static int testFitness(int[] vector) {
        BinarySearchProcessed bin = new BinarySearchProcessed();
        bin.fitness = max_fitness;
        int[] arr = Arrays.copyOfRange(vector, 0, 5);
        //{vector[0], vector[1], vector[2], vector[3], vector[4]}
        bin.search(arr, vector[5]);
        return bin.fitness;
    }

    private static int[][] getNeighbour(int[] vector) {
        int [][] arr_of_arr = new int[6][6];
        for (int change = 0; change < vector.length; ++change) {
            arr_of_arr[change] = randomArrayValue(change, 0, 49, vector);
        }
        return arr_of_arr;
    }

    private static int[][] getChild(int[][] vectors, int[] fitness) {
        int [][] arr_of_arr = new int[6][6];
        int[] cp = Arrays.copyOfRange(fitness, 0, fitness.length);
        int[] list_order = new int[6];
        int[][] sorted_list = new int[6][6];
        int min = 0;
        Arrays.sort(fitness);
        // verbose = true;
        // printArray(fitness);
        int to_find = 0; // 3
        while (to_find < 3) {
            min = fitness[0];
            for (int i = 0; i < 6; i++) {
                if (cp[i] == fitness[to_find]) {
                    sorted_list[to_find] = vectors[i];
                    sorted_list[to_find+3] = vectors[i];
                    to_find +=1;
                    break;
                }
            }
            to_find +=1;
        }
        for (int i = 0; i < 6; i++) {
            // Crossover
            if (Math.random() > 0.2) {
                int split_at = (int)(Math.random() * 5 + 1);
                int split_with = (int)(Math.random() * 5 + 1);
                for (int j = 0; j < 6; j++) {
                    if (j < split_at) {
                        sorted_list[i][j] = sorted_list[split_with][j];
                    } else {
                        sorted_list[split_with][j] = sorted_list[i][j];
                    }
                }
            }
            // Random mutation
            if (Math.random() < 0.1) {
                int mutate_idx = (int)(Math.random() * 6 + 0);
                
                sorted_list[i] = randomArrayValue(mutate_idx, 0, 49, sorted_list[i]);
                // verbose = true;
                // printArray(sorted_list[i]);
            }
        }
        return sorted_list;
    }

    private static void printArray(int[] vector) {
        String out = new String();
        for (int i = 0; i < vector.length; i++) {
            out += vector[i] + ", ";
        }
        if (verbose) { System.out.println(out);};
    }

    private static int[] randomArrayValue(int idx, int min, int max, int[] vector) {
        int[] out = Arrays.copyOfRange(vector, 0, vector.length);
        out[idx] = (int)(Math.random() * max + min);
        return out;
    }

    private static void writeJUnit(int[] vector) {
        List<String> lines =
            Arrays.asList(Arrays.toString(vector));
        Path file = Paths.get("found_array");
        try{
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            // do something
        }

        // writeme
        printArray(vector);
    }

    private static List<String> lines;

    private static int commitChanges(String filename) {
        int line_number = lines.size();
        Path file = Paths.get(filename);
        try{
            Files.write(file, lines, Charset.forName("UTF-8"));
        } catch (IOException e) {
            // do something
            System.out.println("Error writing resources");
        }
        return line_number;
    }

    private static void appendResourceUsage(Boolean found, int cycles,
                                           Boolean append) {
        if (verbose) {
            System.out.println("Found: " + found + "\nCycles consumed: " + cycles);
        }
        int f;
        if (found) {f = 1;} else {f = 0;};
        lines.add(f+","+cycles);
    }

    private static void addHeaders() {
        lines.add("found,cycles");
    }

    private static int[] randomArray(int size) {
        int[] random_array = new int[size]; // length 6 = 5 array, 1 search
        for (int i = 0; i < random_array.length; i++) {
            random_array[i] = (int)(Math.random() * 49);
        }
        return random_array;
    }

    private static int array_size = 6;

    private static int Genetic(boolean append) {
        int[][] random_arrays = new int[6][6];
        int[] fitness_list = new int[6]; // Store the correpsonding fitness value
        for (int i = 0; i < 6; i++) {
            random_arrays[i] = randomArray(array_size);
        }
        int best_fitness = max_fitness;
        Boolean found = false;
        Boolean stop = false;
        int consumed;
        for (consumed = 0;;) {
            if (best_fitness == 0) {
                found = true; stop = true;
                break;
            }
            if (stop) break;
            for (int i = 0; i < 6; i++) {
                fitness_list[i] = testFitness(random_arrays[i]);
                consumed += 1; // consume resources
                if (consumed >= resources) {
                    stop = true;
                    break;
                }
            }
            for (int i = 0; i < 6; i++) {
                best_fitness = Math.min(fitness_list[i],best_fitness);
            }
            random_arrays = getChild(random_arrays, fitness_list);
            // random_arrays = getNeighbour(random_arrays[0]);
        }
        String rs = Integer.toString(resources);
        appendResourceUsage(found, consumed, append);
        return best_fitness;
    }

    private static int HillClimb(boolean append) {
        int[] random_array = randomArray(array_size);
        int best_fitness = max_fitness;
        Boolean found = false;
        Boolean stop = false;
        int consumed;
        for (consumed = 0;;) {
            if (best_fitness == 0) {
                found = true;
                break;
            }
            if (stop) break;
            int[][] neightbours = getNeighbour(random_array);
            for (int i = 0; i < array_size; ++i) {
                if (consumed >= resources) {
                    stop = true;
                    break;
                }
                consumed += 1; // consume resources
                int new_fitness = testFitness(neightbours[i]);
                if (new_fitness < best_fitness) {
                    best_fitness = new_fitness;
                    random_array = neightbours[i];
                }
            }
        }
        if (found) {
            writeJUnit(random_array);
        }
        String rs = Integer.toString(resources);
        appendResourceUsage(found, consumed, append);
        return best_fitness;
    }

    private static int randomGenerator(boolean append) {
        int best_fitness = max_fitness;
        Boolean found = false;
        int consumed;
        int [] test_array = randomArray(array_size); 
        for (consumed = 0; consumed < resources;) {
            consumed += 1;
            best_fitness = Math.min(testFitness(test_array),best_fitness);
            if (best_fitness == 0) {
                found = true;
                break;
            }
            test_array = randomArray(array_size);
        }
        if (found) {
            writeJUnit(test_array);
        }
        String rs = Integer.toString(resources);
        appendResourceUsage(found, consumed, append);
        return best_fitness;
    }

    @Test
    public void testExample() {
        lines = new ArrayList();
        System.out.println("Fitness Hillclimb: " + HillClimb(false));
        lines = new ArrayList();
        System.out.println("Fitness Random: " + randomGenerator(false));
        lines = new ArrayList();
        System.out.println("Fitness Genetic: " + Genetic(false));
    }

    public static void main(String[] args) {
        if (args.length < 1) {
            System.out.println("Usage: java program test size");
        } else {
            if (args.length > 1) {
                resources = Integer.parseInt(args[1]);
            }
            lines = new ArrayList();
            addHeaders();
            for (int i = 0; i < Integer.parseInt(args[0]); i++) {
                HillClimb(true);
            }
            System.out.println("Lines written: " + commitChanges("./outputs/"+resources+"HillClimb.csv"));
            lines = new ArrayList();
            addHeaders();
            for (int i = 0; i < Integer.parseInt(args[0]); i++) {
                randomGenerator(true);
            }
            System.out.println("Lines written: " + commitChanges("./outputs/"+resources+"Random.csv"));
            lines = new ArrayList();
            addHeaders();
            for (int i = 0; i < Integer.parseInt(args[0]); i++) {
                Genetic(true);
            }
            System.out.println("Lines written: " + commitChanges("./outputs/"+resources+"Genetic.csv"));
        }
    }
    private static boolean verbose = false;
}
