with import <nixpkgs> {};
{
    J2me = stdenv.mkDerivation {
    name = "J2me";
    buildInputs = [
      # PACKAGES_PROJECTS_INSERT
      openjdk
      gradle maven # built tools

      # Presentation
       (texlive.combine
       {inherit (texlive)
           scheme-medium # scheme-full small
           graphicx-psmin
           beamertheme-metropolis
           wrapfig ## Org mode
           hyperref ## Org mode
           capt-of ## Org mode
           media9
           pgfopts
           ;
       })
       ghostscript
       librsvg      
       # Data analysis
       julia_05
    ];
    # VARS_PROJECTS_INSERT
    
    SSL_CERT_FILE="/etc/ssl/certs/ca-certificates.crt";
    GIT_SSL_CAINFO="/etc/ssl/certs/ca-certificates.crt";
    # zlib reuqired by DataFrames, libXt by GR
    LD_LIBRARY_PATH="${pkgs.zlib}/lib/:${pkgs.xorg.libXt}/lib/:${pkgs.xorg.libX11}/lib/:${pkgs.xorg.libXrender}/lib/:${pkgs.xorg.libXext}/lib/";
    shellHook = ''
    COMPILE='urxvt -e latexmk -bibtex -xelatex -pvc -pdf -view=none ./Presentation.tex'

    export JULIA_PKGDIR=$(realpath ./.julia_pkgs)
    mkdir -p $JULIA_PKGDIR
    # START_HOOK_PROJECTS_INSERT
    unset http_proxy
    # END_HOOK_PROJECTS_INSERT
    tmuxp load .
    '';
    };
}
